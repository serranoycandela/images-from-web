from selenium import webdriver
from selenium.webdriver import ChromeOptions
import os
import requests
from selenium.webdriver.common.by import By
import time
import csv

options = ChromeOptions()
driver = webdriver.Chrome(options=options)

# a function that receives a key and downloads seed and posibly pod images
def get_frijol(clave):
    #make folder if it does not exist
    this_path = "/home/fidel/GitLab/images-from-web"
    save_path = os.path.join(this_path, "Images")
    os.makedirs(save_path, exist_ok=True)

    # get this frijol page
    url = f"https://www.genesys-pgr.org/10.18730/"+clave
    driver.get(url)

    # wait and scroll
    time.sleep(5)
    driver.execute_script("window.scrollTo(0, 1000);")
    time.sleep(1)

    # get seed and posibly pod image objects
    img_elements = driver.find_elements(By.CLASS_NAME, 'css-pqdqbj')

    for img_element in img_elements:

        # get the name of the picture from attribute title
        print(img_element.get_attribute("srcset"))
        srcset = img_element.get_attribute("srcset")
        filename = img_element.get_attribute("title").replace(" ", "_")

        # construct the url of this frijol picture
        src1 = srcset.split(" ")[0]
        src1 = src1.replace("_thumbs/", "")
        src1 = src1.replace("/200x200", "")
        print(src1)

        # get the picture and save it
        response = requests.get(src1, stream=True)
        img_path = os.path.join(save_path, filename)
        with open(img_path, 'wb') as file:
            for chunk in response.iter_content(chunk_size=1024):
                if chunk:
                    file.write(chunk)

# to test function get_frijol
#k = "JZ4A*"
#get_frijol(k)

# Open file with the list of all frijoles
# cuts: G25557, G23938
with open('/home/fidel/GitLab/images-from-web/genesys-accessions_fromG23938.csv') as file_obj:
#with open('/home/fidel/GitLab/images-from-web/genesys-accessions-v22K7yRg2kW.csv') as file_obj:
    reader_obj = csv.reader(file_obj)
    # Iterate over each row in the csv
    for row in reader_obj:
        if row[1] != "DOI":
            # get the key of this frijol
            k = row[1].replace("10.18730/", "")
            # get images using this key
            get_frijol(k)
